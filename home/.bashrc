export no_proxy="127.0.0.1,localhost,localnets,.lan,.local,.home,/var/run/docker.sock"
export PYPI_URL="https://pypi.org/"
export DNS_IPs_JSON_list='["8.8.8.8"]'
SERVERS_TO_WHITELIST_FOR_SSH="gitlab.com"
DOMAINS_TO_WHITELIST_FOR_SSL_SPACE_SEPARATED="gitlab.com github.com"
REQUESTS_CA_BUNDLE=$HOME/bundled.pem
